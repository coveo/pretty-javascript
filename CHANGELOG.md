<a name="0.1.3"></a>
## [0.1.3](https://github.com/coveo/pretty-javascript/compare/v0.1.2...v0.1.3) (2016-08-17)



<a name="0.1.2"></a>
## [0.1.2](https://github.com/coveo/pretty-javascript/compare/v0.1.1...v0.1.2) (2016-08-17)



<a name="0.1.1"></a>
## [0.1.1](https://github.com/coveo/pretty-javascript/compare/v0.1.0...v0.1.1) (2016-08-17)



<a name="0.1.0"></a>
# [0.1.0](https://github.com/coveo/pretty-javascript/compare/v0.0.8...v0.1.0) (2016-08-17)



<a name="0.0.8"></a>
## [0.0.8](https://github.com/coveo/pretty-javascript/compare/v0.0.7...v0.0.8) (2016-07-04)



<a name="0.0.6"></a>
## [0.0.6](https://github.com/coveo/pretty-javascript/compare/v0.0.5...v0.0.6) (2016-06-22)


### Bug Fixes

* fixes travis ([367aa00](https://github.com/coveo/pretty-javascript/commit/367aa00))
* fixing linter this package ([1cf3bac](https://github.com/coveo/pretty-javascript/commit/1cf3bac))



<a name="0.0.5"></a>
## [0.0.5](https://github.com/coveo/pretty-javascript/compare/v0.0.4...v0.0.5) (2016-06-21)



<a name="0.0.2"></a>
## 0.0.2 (2016-06-09)



